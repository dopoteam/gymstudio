﻿using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace GymStudio.Models
{
    public static class SampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.Exercitiu.Any())
            {
                var popescu = context.Client.Add(
                    new Client { LastName = "Popescu", FirstMidName = "Vasile" }).Entity;
                var ionescu = context.Client.Add(
                    new Client { LastName = "Ionescu", FirstMidName = "Ion" }).Entity;
                var lupulescu = context.Client.Add(
                    new Client { LastName = "Lupulescu", FirstMidName = "Adrian" }).Entity;

                context.Exercitiu.AddRange(
                    new Exercitiu()
                    {
                        Denumire = "Brate",
                        Dificultate = 3,
                        Client = popescu,
                        NumarRepetari = 20,
                    },
                    new Exercitiu()
                    {
                        Denumire = "Picioare",
                        Dificultate = 5,
                        Client = ionescu,
                        NumarRepetari = 10,
                    },
                    new Exercitiu()
                    {
                        Denumire = "Abdomen",
                        Dificultate = 2,
                        Client = lupulescu,
                        NumarRepetari = 30,
                    }
                    
                );
                context.SaveChanges();
            }
        }
    }
}