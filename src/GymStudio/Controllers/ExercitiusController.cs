using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using GymStudio.Models;

namespace GymStudio.Controllers
{
    public class ExercitiusController : Controller
    {
        private ApplicationDbContext _context;

        public ExercitiusController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Exercitius
        public IActionResult Index()
        {
            var applicationDbContext = _context.Exercitiu.Include(e => e.Client);
            return View(applicationDbContext.ToList());
        }

        // GET: Exercitius/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Exercitiu exercitiu = _context.Exercitiu.Single(m => m.ExercitiuID == id);
            if (exercitiu == null)
            {
                return HttpNotFound();
            }

            return View(exercitiu);
        }

        // GET: Exercitius/Create
        public IActionResult Create()
        {
            ViewData["ClientID"] = new SelectList(_context.Set<Client>(), "ClientID", "Client");
            return View();
        }

        // POST: Exercitius/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Exercitiu exercitiu)
        {
            if (ModelState.IsValid)
            {
                _context.Exercitiu.Add(exercitiu);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["ClientID"] = new SelectList(_context.Set<Client>(), "ClientID", "Client", exercitiu.ClientID);
            return View(exercitiu);
        }

        // GET: Exercitius/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Exercitiu exercitiu = _context.Exercitiu.Single(m => m.ExercitiuID == id);
            if (exercitiu == null)
            {
                return HttpNotFound();
            }
            ViewData["ClientID"] = new SelectList(_context.Set<Client>(), "ClientID", "Client", exercitiu.ClientID);
            return View(exercitiu);
        }

        // POST: Exercitius/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Exercitiu exercitiu)
        {
            if (ModelState.IsValid)
            {
                _context.Update(exercitiu);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["ClientID"] = new SelectList(_context.Set<Client>(), "ClientID", "Client", exercitiu.ClientID);
            return View(exercitiu);
        }

        // GET: Exercitius/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Exercitiu exercitiu = _context.Exercitiu.Single(m => m.ExercitiuID == id);
            if (exercitiu == null)
            {
                return HttpNotFound();
            }

            return View(exercitiu);
        }

        // POST: Exercitius/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Exercitiu exercitiu = _context.Exercitiu.Single(m => m.ExercitiuID == id);
            _context.Exercitiu.Remove(exercitiu);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
