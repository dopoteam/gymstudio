﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GymStudio.Models
{
    public class Client
    {
        [ScaffoldColumn(false)]
        public int ClientID { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstMidName { get; set; }

        public virtual ICollection<Exercitiu> Exercitiu { get; set; }
    }
}