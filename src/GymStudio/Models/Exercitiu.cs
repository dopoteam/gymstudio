﻿using System.ComponentModel.DataAnnotations;

namespace GymStudio.Models
{
    public class Exercitiu
    {
        [ScaffoldColumn(false)]
        public int ExercitiuID { get; set; }
        [Required]
        public string Denumire { get; set; }

        public int Dificultate { get; set; }
        [Range(1, 5)]
     
        public int NumarRepetari { get; set; }
        [Range(1, 50)]

        [ScaffoldColumn(false)]
        public int ClientID { get; set; }

        // Navigation property
        public virtual Client Client { get; set; }
    }
}